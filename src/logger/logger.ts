import { Injectable } from '@angular/core';

@Injectable()
export class Logger {
  public info(data: any, ...args: string[]) {
    console.log(data, args);
  }

  public warn(data: any, ...args: string[]) {
    console.warn(data, args);
  }

  public error(data: any, ...args: string[]) {
    console.error(data, args);
  }
}
